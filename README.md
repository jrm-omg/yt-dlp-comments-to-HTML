# yt-dlp-comments-to-HTML

Lame Python command line script that turns yt-dlp downloaded comments to HTML (so you can upload them and automatically translate them, for instance).

## Usage

```sh
python3 c2html.py demo.json
```

## How do I download YouTube comments ?

Using [yt-dlp](https://github.com/yt-dlp/yt-dlp) :

```sh
yt-dlp --skip-download --no-write-info-json --get-comments --print-to-file "%(comments)#j" "%(id)s.comments.json" YOUR_YOUTUBE_URL
```
