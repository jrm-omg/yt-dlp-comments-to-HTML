import sys # args
import json
from datetime import datetime
import re

# Requiring an input file
if len(sys.argv) <= 1:
    print('Please give me an input file (JSON)')
    sys.exit()

# Parsing the input JSON
resultHTML = ''
filename = sys.argv[1]
with open(filename) as f:
    c = f.read()
j = json.loads(c)
for post in j:
    date = datetime.fromtimestamp(post['timestamp']).strftime('%m-%d-%Y %H:%M:%S')
    author = post['author'][1:]
    text = post['text']
    if(post['parent'] != 'root'):
        className = 'thread'
    else:
        className = ''
    resultHTML += f'    <div class="{className}">{date} - {author} - {text}</div>\n'

# Rendering the output
tFile = open('template.html')
tpl = tFile.read()
match = re.search(r'(<!-- Python and the Holy Grail -->)', tpl)
outputHTML = tpl[:match.end()] + '\n' + resultHTML[:-1] + tpl[match.end():]
oFile = open('output.html', 'w')
oFile.write(outputHTML)
oFile.close()
print(f'[✓] {len(j)} HTML comments written to output.html')
